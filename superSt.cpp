#include "superSt.h"
#include <vector>
#include <cstdlib>
#include <sstream>
using namespace std;

// Tokenizes (splits) the SuperString, based on the delimiter
// Parameters: delim: the delimiter
// Returns: a vector of SuperStrings that were split

vector<SuperString> SuperString::tokenize(const string & delim) {
	vector<SuperString> tokens;
	size_t p0 = 0, p1 = string::npos;
	while(p0 != string::npos) {
    	p1 = find_first_of(delim, p0);
    	if(p1 != p0) {
      		string token = substr(p0, p1 - p0);
      		tokens.push_back(token);
      	}
		p0 = find_first_not_of(delim, p1);
	}
	return tokens;
}

// Overload of the operator= for using stataments such as:
// SuperString st = static_cast<string>("3 4 5 + * 1 +");
SuperString& SuperString::operator= (string s) {
	this->assign(s);
	return *this;
}


// Overload of the operator= for using stataments such as:
// SuperString st = 209;
SuperString& SuperString::operator= (int n) {
	this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream()
   		<< std::dec << n ) ).str());
	return *this;
}

// Overload of the constructor, allows statements such as:
// SuperString st(254);
SuperString::SuperString(int a) {
   this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream()
   		<< std::dec << a ) ).str());
}

// Converts the SuperString to int, if possible
bool SuperString::toInt(int& i) {
   char c = this->c_str()[0];
   if(empty() || ((!isdigit(c) && (c != '-') && (c != '+')))) return false ;

   char * p ;
   i = strtol(c_str(), &p, 10) ;

   return (*p == 0) ;
}


// Evaluates the postfix expression.
// Returns:
//   - result: integer through which we return the result of the eval
//   - boolean return value: true if the expression is valid
bool SuperString::evalPostfix(int &result) {

	stack<int> PostStack;

	int operand1, operand2, counter = 0, dummy;

	vector<SuperString> Expression = this->tokenize(" ");

	if(Expression.size() < 3) return false;

	while(counter != Expression.size())
	{
		if(Expression[counter].toInt(dummy))
			PostStack.push(dummy);

		else if(Expression[counter] == "+")
		{
			if(PostStack.empty()) return false;

			operand1 = PostStack.top();
			PostStack.pop();

			if(PostStack.empty()) return false;

			operand2 = PostStack.top();
			PostStack.pop();

			PostStack.push(operand1 + operand2);
		} else if(Expression[counter] == "-")
		{
			if(PostStack.empty()) return false;

			operand1 = PostStack.top();
			PostStack.pop();

			if(PostStack.empty()) return false;

			operand2 = PostStack.top();
			PostStack.pop();

			PostStack.push(operand2 - operand1);
		} else if(Expression[counter] == "*")
		{
			if(PostStack.empty()) return false;

			operand1 = PostStack.top();
			PostStack.pop();

			if(PostStack.empty()) return false;

			operand2 = PostStack.top();
			PostStack.pop();

			PostStack.push(operand1 * operand2);
		} else if(Expression[counter] == "/")
		{
			if(PostStack.empty()) return false;

			operand1 = PostStack.top();
			PostStack.pop();

			if(PostStack.empty()) return false;

			operand2 = PostStack.top();
			PostStack.pop();

			if(operand1 == 0) return false;

			PostStack.push(operand2 / operand1);
		} else return false;

		counter++;
	}

	dummy = PostStack.top();
	PostStack.pop();

	if(!PostStack.empty()) return false;

	result = dummy;

	return true;






}

