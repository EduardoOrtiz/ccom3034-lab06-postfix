#include <iostream>
#include <cassert>
#include "superSt.h"

using namespace std;

void testPostfix();

int main() {
	int a = 0;

	testPostfix();

	SuperString s2 = static_cast<string>("5 5 * 10 7 + +");
	if ( s2.evalPostfix(a)) {
		cout << "a: " << a << endl;
	}

	return 0;
}

void testPostfix()
{
	int a = (3 * 5) + 9 / (3 * 3);
	int b;
	
	SuperString Test1 = (string)("3 3 * 9 / 3 5 * +");
	
	Test1.evalPostfix(b);
	
	assert(a == b);
	
	int c = 0, d = 0;
	
	SuperString Test2 = (string)("1 2 3 4 5 +");
	SuperString Test3 = (string)("+ + 11 22 33");
	
	Test2.evalPostfix(d);
	
	assert(c == d);
	
	Test3.evalPostfix(d);
	
	assert(c == d);
}
